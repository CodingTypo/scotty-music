const Discord = require('discord.js')
module.exports = (client, message, query, tracks, content, collector) => {
    const EmbedCancel = new Discord.MessageEmbed()
        .setColor('#5a005a')
        .setTitle(`[Foutmelding] Gestopt met zoeken`)
        .setDescription(`De selectie is gestopt`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();

    const EmbedCancelValidNumber = new Discord.MessageEmbed()
        .setColor('#5a005a')
        .setTitle(`[Foutmelding] Geen valide gegeven`)
        .setDescription(`Stuur een nummer tussen **1** en **${tracks.length}**, andere nummers werken niet`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();

    if (content === 'cancel') {
        collector.stop();
        return message.channel.send(EmbedCancel);
    } else message.channel.send(EmbedCancelValidNumber);
};
const Discord = require('discord.js');
var afkoelenstop = 0;
var stemmerss = [];
//var stemmers = [];
module.exports = {
  name: 'stop',
  aliases: ['st'],
  category: 'Music',
  utilisation: '{prefix}stop',

execute(client, message, args, ops, track) {
  const voiceChannelID = message.member.voice.channelID;
    if (voiceChannelID === '695964540244066365') {
      const EmbedAFKChannel = new Discord.MessageEmbed()
        .setColor('#e71837')
        .setTitle(`Je zit in het AFK-kanaal`)
        .setDescription(`Je kan hier geen liedjes afspelen. Stap in een ander kanaal om muziek te kunnen luisteren`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();
      message.channel.send(EmbedAFKChannel);
      } else {
      const EmbedNotInChannel = new Discord.MessageEmbed()
        .setColor('#e71837')
        .setTitle(`Je zit niet in een spraakkanaal`)
        .setDescription(`Als je een liedje wil spelen moet je een spraakkanaal joinen`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();
      if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);

      const EmbedBotNotInChannel = new Discord.MessageEmbed()
        .setColor('#e71837')
        .setTitle(`Ik zit nog niet in een spraakkanaal`)
        .setDescription(`Als je een liedje wil spelen moet je een spraakkanaal joinen`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();
      if (!message.guild.me.voice.channel) return message.channel.send(EmbedBotNotInChannel);

      const EmbedNoSongInQueue = new Discord.MessageEmbed()
          .setColor('#fc9303')
          .setTitle(`De wachtrij is leeg!`)
          .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
              **/play [nummer]**`)
          .setURL('https://minecraft.scouting.nl/')
          .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
          .setTimestamp();
      if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);

      const EmbedNotInSameChannel = new Discord.MessageEmbed()
        .setColor('#e71837')
        .setTitle(`Je zit niet in het goede spraakkanaal`)
        .setDescription(`Als je een liedje wil afspelen moet je in het volgende spraakkanaal zitten: **${message.guild.me.voice.channel}**`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();
      if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);
    };
//      console.log(String(stemmers));
//      var luisteraars = message.member.voice.channel.members.size -1;
//        console.log(luisteraars);
//      var auteur = (message.member.id);
//      var alGestemd = false;
//      if (stemmers.includes(message.member.id) == true) {
//        if (required <= stemmers.length) {
//          message.channel.send('Je had al gestemd, maar er zijn nu minder mensen online, dus we gaan skippen!');
//          return client.player.skip(message);
//        } else {
//          return message.channel.send('Je hebt al gestemd!')}
//      };
//      console.log(alGestemd);
//      if (alGestemd == false) {
//        stemmers.push(auteur);
//        message.channel.send(`Je stemde voor skippen ${message.member}!`)};
//      let required = Math.ceil(parseInt(luisteraars)/2);
//      if (required <= stemmers.length) {
//        message.channel.send('Yes we gaan skippen');
//        stemmers = [];
//        return client.player.skip(message);
//        } else {
//        message.channel.send(`Nog niet genoeg skips!! ${stemmers.length}/${required}`);
//        console.log(required);
//        };
//}};

function nieuweDagc(x) {
    if (x >= 86400) {
        return x - 86400
    } else {return x}
};
datum = new Date();
var tijdstipstop = (datum.getHours() * 3600) + (datum.getMinutes() * 60) + datum.getSeconds();

//pak het aantal leden uit het kanaal en trek er 1 van af (dat is de bot, die telt niet mee)
var luisteraars = message.member.voice.channel.members.size -1;

//deel luisteraars door 2, op deze manier moet de helft of alles omhoog stemmen. dus bij 4 mensen moeten 2 stemmen, maar bij 3 mensen ook.
var required = Math.ceil(parseInt(luisteraars)/2);

//pak het ID van degene die /skip stuurt
var auteur = (message.member.id);

//zet de alGestemd variabele om later te checken of de persoon al een keer gestemd heeft.
var alGestemd = false;

//als er nog een cooldown is berichtje sturen dat de persoon die /skip doet ff moet wachten.
//if (tijdstipstop < afkoelenstop) {
//  const EmbedAfkoelen = new Discord.MessageEmbed()
//    .setColor('#fc9303')
//    .setTitle(`Je moet even wachten`)
//    .setDescription("Nog even wachten graag, over **" + (afkoelenstop - tijdstipstop) + "** seconden kan je de muziek weer stoppen")
//    .setURL('https://minecraft.scouting.nl/')
//    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
//    .setTimestamp();
//  return message.channel.send(EmbedAfkoelen)
//} else {afkoelenstop = 0}; //als het tijdstipstop niet kleiner is dan afkoelenstop wordt afkoelenstop op 0 gezet.

//als de ID voorkomt in de "stemmerss" array--> voer optie A of optie B uit
if (stemmerss.includes(message.member.id) == true) {

  //optie A: toch skippen, hier wordt gecheckt of het aantal luisteraars gedeeld door 2 (required) kleiner of gelijk is aan de lengte van de stemmerss array, dit betekend namelijk dat er in de tussentijd mensen uit het kanaal zijn gestapt.
  if (required <= stemmerss.length) {
    const EmbedAlGestemdMaarGoed = new Discord.MessageEmbed()
      .setColor('#00A551')
      .setTitle(`Okey, ik stop de muziek toch maar`)
      .setDescription(`Je had je stem al uitgebracht. Maar er zijn mensen uit het kanaal gestapt, dus ik kan nu toch de muziek stoppen`)
      .setURL('https://minecraft.scouting.nl/')
      .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
      .setTimestamp();
    message.channel.send(EmbedAlGestemdMaarGoed);
    stemmerss = []; //stemmerss array wordt gewist
    afkoelenstop = nieuweDagc(tijdstipstop + 20); //20 seconden cooldown wordt geactiveerd voor /stop
    client.player.setRepeatMode(message, false)
    return client.player.stop(message); //Stop de muziek

  //optie B: Er zijn geen mensen uit het kanaal gestapt en de "required" wordt nogsteeds niet aan voldaan,dus melden dat de persoon al gestemd heeft en afbreken
  } else {
    const EmbedAlGestemd = new Discord.MessageEmbed()
      .setColor('#fc9303')
      .setTitle(`Je hebt al gestemd`)
      .setDescription(`Je hebt je stem al uitgebracht om de muziek te stoppen! **${stemmerss.length}/${required}** stemmen`)
      .setURL('https://minecraft.scouting.nl/')
      .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
      .setTimestamp();
    return message.channel.send(EmbedAlGestemd);
  }};

//als degene die het bericht stuurt niet in de stemmerss zit en dus niet de vorige "if" activeerd--> push het ID van de auteur in de stemmerss array en vertel de auteur dat hij vanaf dat moment gestemd heeft.
if (alGestemd == false) {
  stemmerss.push(auteur);
  const EmbedJeHebtGestemd = new Discord.MessageEmbed()
    .setColor('#00A551')
    .setTitle(`Je hebt gestemd om de muziek te stoppen`)
    .setDescription(`${message.member} heeft gestemd om de muziek te stoppen. Jullie hebben nu **${stemmerss.length}** van de **${required}** nodige stemmen`)
    .setURL('https://minecraft.scouting.nl/')
    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
    .setTimestamp();
//er wordt geskipt als de quorum gehaald is (required <= stemmerss.length)
if (required <= stemmerss.length) {
  const EmbedSkip = new Discord.MessageEmbed()
    .setColor('#00A551')
    .setTitle(`De muziek is gestopt`)
    .setDescription(`Jullie hebben gestemd dat de muziek gestopt wordt. voeg nieuwe liedjes toe met **/play [nummer]**`)
    .setURL('https://minecraft.scouting.nl/')
    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
    .setTimestamp();
  message.channel.send(EmbedSkip);
  stemmerss = []; //stemmerss array wordt gewist
  afkoelenstop = nieuweDagc(tijdstipstop + 20); //20 seconden cooldown wordt geactiveerd voor /stop
  client.player.setRepeatMode(message, false);
  return client.player.stop(message); //stop de muziek
  } else {
    //de member laten weten dat er nog niet genoeg skips zijn.
  message.channel.send(EmbedJeHebtGestemd);
  };
}}};

//  execute(client, message, args, ops, track) {
//    let fetched = ops.active.get(message.guild.id);
//    if (!fetched) return message.channel.send("geen muziek")
//    if (message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send('niet goede kanaal')
//    let userCount = message.member.voice.channel.members.size;
//    let required = Math.ceil(userCount/2);
  //  if (!fetched.queue[0].voteSkips) fetch.queue[0].voteSkips = [];
  //  if (fetched.queue[0].voteSkips.includes(message.member.id)) return message.channel.send(`al een keer gestemd ${fetched.queue[0].voteSkips.length}/${required} benodigd.`)
//    fetched.queue[0].voteSkips.push(message.member.id);
//    ops.active.set(message.guild.id, fetched);
//    if (fetched.queue[0].voteSkips.length >= required) {
//    message.channel.send('geskipt!');
//    return fetched.dispatcher.end();
//    return client.player.skip(message);
//    }
//    message.channel.send(`je hebt gestemd om te skippen! ${fetched.queue[0].voteSkips.length}/${required}`);
//}};
